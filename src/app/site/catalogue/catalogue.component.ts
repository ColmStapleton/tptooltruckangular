import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/shared/services/product.service';
import { Product } from 'src/app/shared/classes/product';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss']
})
export class CatalogueComponent implements OnInit {

  products: Array<Product> = new Array<Product>();
  selectedType: string;
  selectedFamily: string;
  searchFilter: string;
  isVisible = true;

  constructor(private _productService: ProductService) {
    this._productService.GetProducts().subscribe((data) => {
      this.products = data as Product[];
      console.log(this.products);
    });
  }

  ngOnInit() {
  }

  hideImages() {
    this.isVisible = ! this.isVisible;
  }

  getProducts(): Array<Product> {
    if (this.searchFilter != null) {
      if (this.searchFilter.length > 0) {
        return this.searchFilter === '' ? this.products : this.products.filter
        (product => product.productName.toLowerCase().match(this.searchFilter.toLowerCase()));
      } else {
        return this.products;
      }
    } else {
      return this.products;
    }
  }

  getTypes(): string[] {
    const types: Array<string> = new Array<string>();
    this.products.forEach(product => {
      if (!types.includes(product.productType)) {
        types.push(product.productType);
      }
    });
    return types;
  }

  filterTypes(type: string) {
    this.selectedType = type;
  }
}
