import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccueilComponent } from './site/accueil/accueil.component';
import { CatalogueComponent } from './site/catalogue/catalogue.component';


const routes: Routes = [
  {path: 'accueil', component: AccueilComponent},
  {path: 'catalogue', component: CatalogueComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
